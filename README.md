# Person Lib

### Repo
[Follow by link](https://gitlab.com/volodymyrkr/person-lib.git)

### Project init

- Init repository

```
git init
```

- Init package manager to generate ```package.json```

```
npm init -y
```

- Create remote repository ```https://gitlab.com/volodymyrkr/person-lib.git```

- Add remote repository to local
```
git remote add origin git@gitlab.com:volodymyrkr/person-lib.git
```

- Update package manager configuration file ```package.json```

```
  "main": "dist/index.js",
  "types": "dist/index.d.ts",
  ...
  "private": false,
  "repository": "https://gitlab.com/volodymyrkr/person-lib.git",
  ...
```

- Install Typescript

```
npm i -D typescript
npm i -g typescript
```

- Init Typescript to generate ```tsconfig.json```

```
tsc --init
```

- Update compilerOptions ```tsconfig.json``` 

```
  "compilerOptions": {
    "declaration": true,        /* Generates corresponding '.d.ts' file. */
    "sourceMap": true,          /* Generates corresponding '.map' file. */
    "outDir": "./dist",         /* Redirect output structure to the directory. */
    ...
  }
```

- Add other parameters ```tsconfig.json``` 
```
  "exclude": [
    "node_modules"
  ],
  "include": [
    "src/**/*"
  ]
```

- Push commit into master local and remote

```
git add .
git commit -m "Initial commit"
git push -u origin master
git tag v0.0.0
git push --tags 
```

- Publish package

```
  npm login
  npm publish
```