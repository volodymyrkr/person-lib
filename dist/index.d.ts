export declare enum ConsoleLogColorType {
    BLACK = 0,
    RED = 1,
    BLUE = 2,
    GREEN = 3,
    GREY = 4
}
export declare const mapConsoleLogColorTypeToColor: (colorType: ConsoleLogColorType) => string;
