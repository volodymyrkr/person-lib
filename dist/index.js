"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ConsoleLogColorType;
(function (ConsoleLogColorType) {
    ConsoleLogColorType[ConsoleLogColorType["BLACK"] = 0] = "BLACK";
    ConsoleLogColorType[ConsoleLogColorType["RED"] = 1] = "RED";
    ConsoleLogColorType[ConsoleLogColorType["BLUE"] = 2] = "BLUE";
    ConsoleLogColorType[ConsoleLogColorType["GREEN"] = 3] = "GREEN";
    ConsoleLogColorType[ConsoleLogColorType["GREY"] = 4] = "GREY";
})(ConsoleLogColorType = exports.ConsoleLogColorType || (exports.ConsoleLogColorType = {}));
exports.mapConsoleLogColorTypeToColor = function (colorType) {
    switch (colorType) {
        case ConsoleLogColorType.RED:
            return "#CC0000";
        case ConsoleLogColorType.BLUE:
            return "#0000CC";
        case ConsoleLogColorType.GREEN:
            return "#00CC00";
        case ConsoleLogColorType.GREY:
            return "#CCCCCC";
        case ConsoleLogColorType.BLACK:
        default:
            return "#000000";
    }
};
//# sourceMappingURL=index.js.map