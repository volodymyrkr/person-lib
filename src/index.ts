export enum ConsoleLogColorType {
  BLACK,
  RED,
  BLUE,
  GREEN,
  GREY
}

export const mapConsoleLogColorTypeToColor = (colorType: ConsoleLogColorType): string => {
  switch (colorType) {
    case ConsoleLogColorType.RED:
      return "#CC0000";
    case ConsoleLogColorType.BLUE:
      return "#0000CC";
    case ConsoleLogColorType.GREEN:
      return "#00CC00";
    case ConsoleLogColorType.GREY:
      return "#CCCCCC";
    case ConsoleLogColorType.BLACK:
    default:
      return "#000000";
  }
};
